module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),


    autoprefixer: {
      options: {
        browsers: ['last 3 version']
      },
      multiple_files: {
        expand: true,
        flatten: true,
        src: 'css/src/*.css',
        dest: 'css/build/prefixed/'
      }
    },

    cssmin: {
      combine: {
        files: {
          'css/build/minified/global.css': ['css/build/prefixed/*.css']
        }
      }
    },

    jshint: {
      beforeconcat: ['js/src/main.js']
    },

    concat: {
      dist: {
        src: [
          'js/src/plugins.js',
          'js/src/main.js'
        ],
        dest: 'js/build/main.js'
      }
    },

    uglify: {
      build: {
        src: 'js/build/main.js',
        dest: 'js/build/main.min.js'
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'img/'
        }]
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      scripts: {
        files: ['js/src/*.js'],
        tasks: ['concat', 'uglify', 'jshint'],
        options: {
          spawn: false,
        }
      },
      css: {
        files: ['css/src/*.css'],
        tasks: ['autoprefixer', 'cssmin'],
        options: {
          spawn: false,
        },
      },
      html: {
      	files: ['*.html'],
      	options: {
      	  spawn: false,
        },
      },
      
      
      },
    

    connect: {
      server: {
        options: {
          port: 8000,
          base: './',
          hostname: '*'
        }
      }
    },

  });

  require('load-grunt-tasks')(grunt);

  // Default Task is basically a rebuild
  grunt.registerTask('default', ['concat', 'uglify', 'imagemin']);

  grunt.registerTask('dev', ['connect', 'watch']);

};